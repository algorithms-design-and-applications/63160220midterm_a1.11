/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.wariya.a1_11;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 *
 * @author wariy
 */
public class A1_11 {

     public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter element array(positive integer): ");
        String arrString = input.nextLine();
        String[] arr = arrString.split(",");
        ArrayList<Integer> a = new ArrayList<Integer>();
        for (String string : arr) {
            a.add(Integer.parseInt(string));
        }
        int result = findX(a);
        System.out.println(result);
    }

    public static int findX(ArrayList<Integer> a) {
        ArrayList<Integer> copyA = new ArrayList<>(a);
        for (int i : a) {
            copyA.removeAll(Collections.singletonList(i));
            if (Math.abs(a.size() - copyA.size()) != 2) {
                return i;
            }
            copyA.removeAll(copyA);
            copyA.addAll(a);
        }
        return -1;
    }
}
